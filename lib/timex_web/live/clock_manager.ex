defmodule TimexWeb.ClockManager do
  use GenServer

  def init(ui) do
    :gproc.reg({:p, :l, :ui_event})
    {_, now} = :calendar.local_time()

    Process.send_after(self(), :working, 1000)
    {:ok, %{ui_pid: ui, time: Time.from_erl!(now), mode: Time, st1: Working, alarm: Time.add(Time.from_erl!(now),60), st2: Idle, count: 0, show: true, selection: nil, te2e: nil}}
  end

  def handle_info(:working, %{ui_pid: _ui, time: alarm, mode: Time, alarm: alarm} = state) do
    :gproc.send({:p, :l, :ui_event}, :start_alarm)
    Process.send_after(self(), :working, 1000)
    alarm = Time.add(alarm, 1)
    {:noreply, state |> Map.put(:time, alarm) }
   end

  def handle_info(:working, %{ui_pid: ui, mode: mode, time: time, st1: Working} = state) do
    Process.send_after(self(), :working, 1000)
    time = Time.add(time, 1)
    if mode == Time do
      GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    end
    {:noreply, state |> Map.put(:time, time) }
  end

  def handle_info(:"top-left", %{mode: Time} = state) do
    IO.puts("Mode SWatch")
    {:noreply, %{state | mode: SWatch}}
  end

  def handle_info(:"top-left", %{mode: SWatch, ui_pid: ui, time: time } = state) do
    IO.puts("Mode Time")
    GenServer.cast(ui, {:set_time_display, Time.truncate(time, :second) |> Time.to_string })
    {:noreply, %{state | mode: Time}}
  end

  def handle_info(:"bottom-right", %{st2: Idle, mode: Time} = state) do
    Process.send_after(self(),Waiting2Edit , 250)
    {:noreply, %{state | st2: Waiting}}
   end

  def handle_info(:"bottom-right", %{st2: Waiting, mode: Time} = state) do
    IO.puts("ME FUI")
    { :noreply, %{state | st2: Idle} }
  end

  def handle_info(Waiting2Edit, %{ st2: Waiting, ui_pid: ui } = state) do
    IO.puts("YA LLEGUE")
    te2e = Process.send_after(self(),Editing2Editing, 250)
    :gproc.send({:p, :l, :ui_event}, :stop_clock)
    {:noreply, %{state | st2: Editing, count: 0, show: true, selection: Hour, te2e: te2e}}
  end

  def handle_info(Editing2Editing,%{ui_pid: ui, time: time, st2: Editing, count: count , show: show, selection: selection , te2e: te2e }=state) do
    Process.cancel_timer(te2e)

      if count < 20 do
        te2e = Process.send_after(self(),Editing2Editing, 250)
        show = !show
        count = count + 1
        GenServer.cast(ui, {:set_time_display, format(time, show, selection)})
        {:noreply, %{state | st2: Editing,  count: count , show: show, te2e: te2e }}
      else
        GenServer.cast(ui, {:set_time_display, format(time, true, selection)})
        :gproc.send({:p, :l, :ui_event}, :resume_clock)
        {:noreply, %{state | st2: Idle,  count: count , show: show, te2e: te2e }}
      end
  end

  def handle_info(:"bottom-right", %{ui_pid: ui, time: time, st2: Editing, selection: selection , te2e: te2e}=state) do
    Process.cancel_timer(te2e)
    te2e = Process.send_after(self(),Editing2Editing, 250)
    selection = case selection do
      Hour -> Minute
      Minute -> Second
      _ -> Hour
    end
    GenServer.cast(ui, {:set_time_display, format(time, true, selection)})
    {:noreply, %{state | st2: Editing, count: 0,show: true, selection: selection, te2e: te2e }}
  end

  def handle_info(:"bottom-left", %{st2: Editing, time: time, selection: selection, count: count}=state) do
    Process.send_after(self(), :working, 1000)
    time = case selection do
      Hour ->     Time.add(time, 3600)
      Minute ->   Time.add(time, 60)
      _ -> Time.add(time, 1)
    end

    {:noreply, %{state | st2: Editing ,selection: selection, time: time  }}
  end

  def format(time,show,selection)do
      if(show)do
        "#{time.hour}:#{time.minute}:#{time.second}"
      else
        case selection do
        Hour ->   "  :#{time.minute}:#{time.second}"
        Minute ->  "#{time.hour}:  :#{time.second}"
        _      ->  "#{time.hour}:#{time.minute}:  "
        end
      end
  end

  def handle_info(_event, state), do: {:noreply, state}
end
